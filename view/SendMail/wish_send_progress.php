<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>WISH SMTP</title>

    <!-- Bootstrap core CSS -->
    <link href="../../css/css_dashgum/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../../font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../css/css_dashgum/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="../../js/js_dashgum/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="../../lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="../../css/css_dashgum/style.css" rel="stylesheet">
    <link href="../../css/css_dashgum/style-responsive.css" rel="stylesheet">

    <script src="../../js/js_dashgum/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo"><b>WISH SMTP</b></a>
            <!--logo end-->
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="login.html">Logout</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <!-- User ID area -->
              	  <h5 class="centered" id="user_id"></h5>

				  <!-- send mail menu -->
                  <li class="mt">
                      <a href="http://58.150.60.107/SMTP/view/SendMail/wish_send_station.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Send Mail</span>
                      </a>
                  </li>
                  
                  <!-- sending progress menu -->
				  <li class="sub-menu dcjq-parent-li">
					  <a class="active" href="http://58.150.60.107/SMTP/view/SendMail/wish_send_progress.php" >
					      <i class="fa fa-desktop"></i>
					      <span>Sending Progress/Result</span>
					  </a>
				  </li>                  

              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

              <div class="row">
                  <div class="col-lg-12 main-chart">
                  
                      <div class="row mt">
                      <!-- SERVER STATUS PANELS -->
                      	<div class="col-md-3 col-sm-3 mb">
                      		<div class="white-panel pn donut-chart">
                      			<div class="white-header">
						  			<h5>Mail Send Progress</h5>
                      			</div>
								<div class="row">
									<div class="col-sm-6 col-xs-6 goleft">
										<p id="sending_percentage"><i class="fa fa-database"></i> 70%</p>
									</div>
	                      		</div>
								<canvas id="serverstatus01" height="120" width="120"></canvas>
								<script>
									var data_1 = 90;
									var data_2 = 50;								
								
									var doughnutData = [
											{
												value: data_1,
												color:"#68dff0"
											},
											{
												value : data_2,
												color : "#fdfdfd"
											}
										];
										var myDoughnut = new Chart(document.getElementById("serverstatus01").getContext("2d")).Doughnut(doughnutData);
								</script>
	                      	</div><! --/grey-panel -->
	                      	
	                      	
                      	</div><!-- /col-md-4-->
	                  	
	                  	<div class="row mt">
	                  		<div class="col-md-2 col-sm-2 box0">
	                  			<!-- sending count -->
	                  			<div class="box1">
						  			<span class="li_cloud"></span>
						  			<h3 id="mail_sending"></h3>
	                  			</div>
						  			<p>보낸 메일의 수</p>
	                  		</div>
	                  		<div class="col-md-2 col-sm-2 box0">
	                  			<!-- sending queue -->
	                  			<div class="box1">
						  			<span class="li_stack"></span>
						  			<h3 id="mail_queue"></h3>
	                  			</div>
						  			<p>남아있는 메일의 수</p>
	                  		</div>
	                  	</div><!-- /row mt -->
	                  		
                      </div><!-- /row -->
                  </div><!-- /col-lg-9 END SECTION MIDDLE -->
                  
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
                  
              </div><! --/row -->
          </section>
          <!--footer start-->
          <footer class="site-footer">
              <div class="text-center">
                  WISHCOMPANY
                  <a href="http://58.150.60.107/SMTP/view/wish_send_station.php#" class="go-top">
                      <i class="fa fa-angle-up"></i>
                  </a>
              </div>
          </footer>
          <!--footer end-->
      </section>
      <!--main content end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="../../js/js_dashgum/jquery.js"></script>
    <script src="../../js/js_dashgum/jquery-1.8.3.min.js"></script>
    <script src="../../js/js_dashgum/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../js/js_dashgum/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../js/js_dashgum/jquery.scrollTo.min.js"></script>
    <script src="../../js/js_dashgum/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../js/js_dashgum/jquery.sparkline.js"></script>

    <!--common script for all pages-->
    <script src="../../js/js_dashgum/common-scripts.js"></script>
    <script type="text/javascript" src="../../js/js_dashgum/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="../../js/js_dashgum/gritter-conf.js"></script> 
    
    <!--script for this page-->
    <script src="../../js/js_dashgum/sparkline-chart.js"></script>    
	<script src="../../js/js_dashgum/zabuto_calendar.js"></script>	

	<!-- daniel custom js file -->
	<script src="../../js/get_userinfo.js"></script>
  
  

  </body>
</html>
