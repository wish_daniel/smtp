<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko"  xml:lang="ko">
<meta http-equiv="Content-Type" Content="text/html; charset=utf-8" />
<!-- login check -->
<?php
session_start();
if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name'])) {
    echo "<meta http-equiv='refresh' content='0;url=login.html'>";
    exit;
}
$user_id = $_SESSION['user_id'];
$user_name = $_SESSION['user_name'];
$user_privilige = $_SESSION['user_privilige'];
?>
	
	<head>
		<title>wish_SMTP</title>
	</head>
	<body>
	<header>
		<div style="display: inline-block;">
	              Mailing BackOffice
		</div>
		<button class="btn" id="code_pen"><a href="http://codepen.io/pen/?editors=1000">Check HTML Code</a></button>
	</header>
		<div id="main_container">
			<div>
                <div class="form-group" style="width:300px;display: inline-block;">
                  <label for="title">Title</label>
                  <input class="form-control" type="text" id="title"></textarea>
                </div>
            </div>
            
            <form enctype="multipart/form-data" action="http://58.150.60.107/SMTP/controller/upload_file.php" method="POST">
			    <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
			    이 파일을 전송합니다: <input name="userfile" type="file" />
			    <input type="submit" value="파일 전송" />
			</form>
            
			<div>
                <div class="form-group" style="width:1000px;display: inline-block;">
                  <label for="query">Content</label>
                  <textarea class="form-control" rows="10" id="target_html"></textarea>
                </div>
            </div>
            
            <div>
                <div style="display: inline-block;margin-left: 30px;">
                     <div style="margin-bottom: 15px;">send test mail - mailing member</div>
                     <button class="btn" id="test_send_ori">test mailing members<br>[Minhee, Jake, Victor, Karen, Daniel]</button>
                </div>
                <div style="display: inline-block;margin-left: 200px;">
                    <div style="margin-bottom: 25px;">send test mail - OBG members(except ryan)</div>   
                    <button class="btn" id="smtp_test">test OBG</button>
                </div>
            </div>
            
			
			<div style="margin-top: 50px;">
    			<div class="form-group" style="width:500px;display: inline-block;">
    			  <label for="query">Target Query</label>
    			  <textarea class="form-control" rows="3" id="target_query"></textarea>
    			</div>
            </div>
            
            
            <div style="display: inline-block;">
    			<div class="form-group" style="width:300px;">
    			  <label for="query">Password</label>
    			  <input class="form-control" type="password" id="smtp_password"></textarea>
    			</div>
    			<button class="btn" id="send_mail">Send Real User</button>
			</div>

		</div>

	<!-- jquery CDN 적용 -->
	<script src="https://code.jquery.com/jquery-2.2.0.js"></script>
	 
	</script>
	<!-- 부트스트랩 사용하기위한 CDN -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">    
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<!-- daniel's css -->
	<link rel="stylesheet" href="../../css/MailingBO.css">
	<!-- daniel's script code -->
	<script type="text/javascript" src="../../js/smtp_sender.js"></script>
	<script type="text/javascript" src="../../js/test_js/smtp_test.js"></script>

	</body>
</html>