<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>WISH SMTP</title>

    <!-- Bootstrap core CSS -->
    <link href="../../css/css_dashgum/bootstrap.css" rel="stylesheet">
    <!--external css-->
    <link href="../../font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../../css/css_dashgum/zabuto_calendar.css">
    <link rel="stylesheet" type="text/css" href="../../js/js_dashgum/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="../../lineicons/style.css">    
    
    <!-- Custom styles for this template -->
    <link href="../../css/css_dashgum/style.css" rel="stylesheet">
    <link href="../../css/css_dashgum/style-responsive.css" rel="stylesheet">

    <script src="../../js/js_dashgum/chart-master/Chart.js"></script>
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!-- **********************************************************************************************************************************************************
      TOP BAR CONTENT & NOTIFICATIONS
      *********************************************************************************************************************************************************** -->
      <!--header start-->
      <header class="header black-bg">
              <div class="sidebar-toggle-box">
                  <div class="fa fa-bars tooltips" data-placement="right" data-original-title="Toggle Navigation"></div>
              </div>
            <!--logo start-->
            <a href="index.html" class="logo"><b>WISH SMTP</b></a>
            <a class="logo"><div id="active" style="color:red;">&nbsp;non-active</div></a>
            <!--logo end-->
            <div class="top-menu">
            	<ul class="nav pull-right top-menu">
                    <li><a class="logout" href="login.html">Logout</a></li>
            	</ul>
            </div>
        </header>
      <!--header end-->
      
      
      <!-- **********************************************************************************************************************************************************
      MAIN SIDEBAR MENU
      *********************************************************************************************************************************************************** -->
	  <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">
              
              	  <!-- User ID area -->
              	  <h5 class="centered" id="user_id"></h5>

              	  <!-- 발송 진행상태 확인 표시 -->

				  <!-- send mail menu -->
                  <li class="mt">
                      <a class="active" href="http://58.150.60.107/SMTP/view/SendMail/wish_send_station.php">
                          <i class="fa fa-dashboard"></i>
                          <span>Send Mail</span>
                      </a>
                  </li>
                  
              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->
      <!-- **********************************************************************************************************************************************************
      MAIN CONTENT
      *********************************************************************************************************************************************************** -->
      <!--main content start-->
      <section id="main-content">
          <section class="wrapper">

              <div class="row">
                  <div class="col-lg-12 main-chart">
                  
                    <!--
					****************************************************************************************************	
					Target_1 Start
					****************************************************************************************************
					-->
                  
                  	<!-- row_1 input area start -->
					<div class="row mt">
						<!-- mail title input area -->
						<div class="col-md-4 col-sm-4">
							<h4>
								<i class="fa fa-angle-right"></i>
								Insert Mail Title
							</h4>
							
							<div class="form-group">
	                              <div class="col-sm-7">
	                                  <input id="input_mail_title_1" type="text" class="form-control " placeholder="insert title">
	                              </div>
	                              <div style="display: none;">1</div><button type="button" id="send_title_1" class="btn btn-default DisplayInline">apply</button>
	                        </div>
						</div>
						<!-- mail title input area end -->
						
						<!-- target_1 upload start -->
						<div class="col-md-4 col-sm-4">
							<h4>
								<i class="fa fa-angle-right"></i>
								Tartget_1 User CSV Upload
							</h4>
							<!-- submit_1 -->
							<form enctype='multipart/form-data' action='uploadtest.php' method='post'>
							<input size='50' type='file' name='filename' class="DisplayInline">
							<input type='submit' name='submit_1' value='Upload' class="DisplayInline"></form>
						</div>
						<!-- target_1 upload end -->
						
						<!-- file upload_1 start -->
						<div class="col-md-4 col-sm-4">
							<h4>
								<i class="fa fa-angle-right"></i>
								Target_1 Mail Contents Upload
							</h4>
							
				            <form enctype="multipart/form-data" action="http://58.150.60.107/SMTP/controller/upload_file.php" method="POST">
							    <input type="hidden" name="MAX_FILE_SIZE" value="" />
							    <input name="target_1" type="file" class="DisplayInline" />
							    <input type="submit" value="파일 전송" />
							</form>
						</div>						
						<!-- file upload end -->
						
					  </div>
					  
					  <!-- row_1 input area end -->

                  	<!-- row_1 output area start -->
					<div class="row mt">
						<!-- sending data result area start -->
						<div class="col-md-5 col-sm-5 DisplayInline">
							<h4>
								<i class="fa fa-angle-right"></i>
								Sending Data List
							</h4>
							<div class="form-group">
	                              <div class="col-sm-20">
									<h10>
										<div class="DisplayInline">target_1 title : &nbsp;</div><p class="DisplayInline" id="output_mail_title_1"></p>
									</h10>	                                  
	                              </div>
	                              <div class="col-sm-20">
									<h10>
										<div class="DisplayInline">target_1 count : &nbsp;</div><p class="DisplayInline" id="output_target_cnt_1"></p>
									</h10>	                                  
	                              </div>
	                              <div class="col-sm-20">
									<h10>
										<div class="DisplayInline">target_1 mail contents name : &nbsp;</div><div class="DisplayInline" id="output_filename_target_1"></div>
									</h10>	                                  
	                              </div>
	                              <div style="margin-bottom:10px;"></div>
	                              <button type="button" id="test_send_1" class="btn btn-default">Test Mail Send[victor & karen]</button>
	                              <button type="button" id="obg_send_1" class="btn btn-default">OBG Mail Send[except ryan]</button>
	                        </div>
						</div>
						<!-- sending data result area end -->

					  </div>
					  <!-- row_1 output area end -->
						
					<hr>
					
					<!--
					****************************************************************************************************	
					Target_2 Start
					****************************************************************************************************
					-->

                  	<!-- row_2 input area start -->
					<div class="row mt">
						<!-- mail title input area -->
						<div class="col-md-4 col-sm-4">
							<h4>
								<i class="fa fa-angle-right"></i>
								Insert Mail Title
							</h4>
							
							<div class="form-group">
	                              <div class="col-sm-7">
	                                  <input id="input_mail_title_2" type="text" class="form-control " placeholder="insert title">
	                              </div>
	                              <div style="display: none;">2</div><button type="button" id="send_title_2" class="btn btn-default DisplayInline">apply</button>
	                        </div>
						</div>
						<!-- mail title input area end -->
						
						<!-- target_2 upload start -->
						<div class="col-md-4 col-sm-4">
							<h4>
								<i class="fa fa-angle-right"></i>
								Tartget_2 User CSV Upload
							</h4>
							<!-- submit_2 -->
							<form enctype='multipart/form-data' action='uploadtest.php' method='post'>
							<input size='50' type='file' name='filename' class="DisplayInline">
							<input type='submit' name='submit_2' value='Upload' class="DisplayInline"></form>
						</div>
						<!-- target_2 upload end -->
						
						<!-- file upload_2 start -->
						<div class="col-md-4 col-sm-4">
							<h4>
								<i class="fa fa-angle-right"></i>
								Target_2 Mail Contents Upload
							</h4>
							
				            <form enctype="multipart/form-data" action="http://58.150.60.107/SMTP/controller/upload_file.php" method="POST">
							    <input type="hidden" name="MAX_FILE_SIZE" value="" />
							    <input name="target_2" type="file" class="DisplayInline" />
							    <input type="submit" value="파일 전송" />
							</form>
						</div>						
						<!-- file upload end -->
						
					  </div>
					  
					  <!-- row_2 input area end -->

                  	<!-- row_2 output area start -->
					<div class="row mt">
						<!-- sending data result area start -->
						<div class="col-md-5 col-sm-5 DisplayInline">
							<h4>
								<i class="fa fa-angle-right"></i>
								Sending Data List
							</h4>
							<div class="form-group">
	                              <div class="col-sm-20">
									<h10>
										<div class="DisplayInline">target_2 title : &nbsp;</div><p class="DisplayInline" id="output_mail_title_2"></p>
									</h10>	                                  
	                              </div>
	                              <div class="col-sm-20">
									<h10>
										<div class="DisplayInline">target_2 count : &nbsp;</div><p class="DisplayInline" id="output_target_cnt_2"></p>
									</h10>	                                  
	                              </div>
	                              <div class="col-sm-20">
									<h10>
										<div class="DisplayInline">target_2 mail contents name : &nbsp;</div><div class="DisplayInline" id="output_filename_target_2"></div>
									</h10>	                                  
	                              </div>
	                              <div style="margin-bottom:10px;"></div>
	                              <button type="button" id="test_send_2" class="btn btn-default">Test Mail Send[victor & karen]</button>
	                              <button type="button" id="obg_send_2" class="btn btn-default">OBG Mail Send[except ryan]</button>
	                        </div>
						</div>
						<!-- sending data result area end -->

					  </div>
					  <!-- row_2 output area end -->
						
					<hr>
					  
					  <!--
					  ****************************************************************************************************	
					  Target_3 Start
					  ****************************************************************************************************
					  -->					  						

					<!-- row_2 input area start -->
					<div class="row mt">
						<!-- mail title input area -->
						<div class="col-md-4 col-sm-4">
							<h4>
								<i class="fa fa-angle-right"></i>
								Insert Mail Title
							</h4>
							
							<div class="form-group">
	                              <div class="col-sm-7">
	                                  <input id="input_mail_title_3" type="text" class="form-control " placeholder="insert title">
	                              </div>
	                              <div style="display: none;">3</div><button type="button" id="send_title_3" class="btn btn-default DisplayInline">apply</button>
	                        </div>
						</div>
						<!-- mail title input area end -->
						
						<!-- target_2 upload start -->
						<div class="col-md-4 col-sm-4">
							<h4>
								<i class="fa fa-angle-right"></i>
								Tartget_3 User CSV Upload
							</h4>
							<!-- submit_2 -->
							<form enctype='multipart/form-data' action='uploadtest.php' method='post'>
							<input size='50' type='file' name='filename' class="DisplayInline">
							<input type='submit' name='submit_3' value='Upload' class="DisplayInline"></form>
						</div>
						<!-- target_2 upload end -->
						
						<!-- file upload_2 start -->
						<div class="col-md-4 col-sm-4">
							<h4>
								<i class="fa fa-angle-right"></i>
								Target_3 Mail Contents Upload
							</h4>
							
				            <form enctype="multipart/form-data" action="http://58.150.60.107/SMTP/controller/upload_file.php" method="POST">
							    <input type="hidden" name="MAX_FILE_SIZE" value="" />
							    <input name="target_3" type="file" class="DisplayInline" />
							    <input type="submit" value="파일 전송" />
							</form>
						</div>						
						<!-- file upload end -->
						
					  </div>
					  
					  <!-- row_2 input area end -->

                  	<!-- row_2 output area start -->
					<div class="row mt">
						<!-- sending data result area start -->
						<div class="col-md-5 col-sm-5 DisplayInline">
							<h4>
								<i class="fa fa-angle-right"></i>
								Sending Data List
							</h4>
							<div class="form-group">
	                              <div class="col-sm-20">
									<h10>
										<div class="DisplayInline">target_3 title : &nbsp;</div><p class="DisplayInline" id="output_mail_title_3"></p>
									</h10>	                                  
	                              </div>
	                              <div class="col-sm-20">
									<h10>
										<div class="DisplayInline">target_3 count : &nbsp;</div><p class="DisplayInline" id="output_target_cnt_3"></p>
									</h10>	                                  
	                              </div>
	                              <div class="col-sm-20">
									<h10>
										<div class="DisplayInline">target_3 mail contents name : &nbsp;</div><div class="DisplayInline" id="output_filename_target_3"></div>
									</h10>	                                  
	                              </div>
	                              <div style="margin-bottom:10px;"></div>
	                              <button type="button" id="test_send_3" class="btn btn-default">Test Mail Send[victor & karen]</button>
	                              <button type="button" id="obg_send_3" class="btn btn-default">OBG Mail Send[except ryan]</button>
	                        </div>
						</div>
						<!-- sending data result area end -->

					  </div>
					  <!-- row_2 output area end -->
						  
					  <hr>
					
					  <div style="margin-bottom:10px;"></div>
	                  <div class="col-sm-2">
	                  	<input id="send_password" type="password" class="form-control " placeholder="insert password">
					  </div>
	                  <button type="button" id="send_mail" class="btn btn-default">Send Mail</button>	  
						  
                    </div><!-- /row -->
                  </div><!-- /col-lg-9 END SECTION MIDDLE -->
                  
                  
                  
      <!-- **********************************************************************************************************************************************************
      RIGHT SIDEBAR CONTENT
      *********************************************************************************************************************************************************** -->                  
                  
                  
              </div><! --/row -->
          </section>
          <!--footer start-->
          <footer class="site-footer">
              <div class="text-center">
                  WISHCOMPANY
                  <a href="http://58.150.60.107/SMTP/view/wish_send_station.php#" class="go-top">
                      <i class="fa fa-angle-up"></i>
                  </a>
              </div>
          </footer>
          <!--footer end-->
      </section>
      <!--main content end-->
  </section>


    <!-- js placed at the end of the document so the pages load faster -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <script src="../../js/js_dashgum/jquery.js"></script>
    <!-- <script src="../../js/js_dashgum/jquery-1.8.3.min.js"></script> -->
    <script src="../../js/js_dashgum/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="../../js/js_dashgum/jquery.dcjqaccordion.2.7.js"></script>
    <script src="../../js/js_dashgum/jquery.scrollTo.min.js"></script>
    <script src="../../js/js_dashgum/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="../../js/js_dashgum/jquery.sparkline.js"></script>


	<!-- daniel custom js file -->
	<script src="../../js/get_file_info.js"></script>
	<script src="../../js/get_userinfo.js"></script>
    <!--common script for all pages-->
    <script src="../../js/js_dashgum/common-scripts.js"></script>
    <script type="text/javascript" src="../../js/js_dashgum/gritter/js/jquery.gritter.js"></script>
    <script type="text/javascript" src="../../js/js_dashgum/gritter-conf.js"></script> 
    
    <!--script for this page-->
    <script src="../../js/js_dashgum/sparkline-chart.js"></script>    
	<script src="../../js/js_dashgum/zabuto_calendar.js"></script>	
	
	<!-- bootstrap.js below is only needed if you wish to the feature of viewing details
	     of text file preview via modal dialog -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js" type="text/javascript"></script>
	
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
	<link href="../../css/css_dashgum/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
	<link href="../../css/MailingBO.css" rel="stylesheet" type="text/css" />
	
	<!-- canvas-to-blob.min.js is only needed if you wish to resize images before upload.
	     This must be loaded before fileinput.min.js -->
	<script src="../../js/js_fileupload/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
	<script src="../../js/js_fileupload/fileinput.min.js"></script>
	<!-- optionally if you need translation for your language then include 
	    locale file as mentioned below -->
	<script src="../../js/js_fileupload/fileinput_locale_LANG.js"></script>
	<!-- daniel's script code -->
	<script type="text/javascript" src="../../js/smtp_sender.js"></script>
	<script type="text/javascript" src="../../js/test_js/smtp_test.js"></script>
	<script type="text/javascript" src="../../js/get_active.js"></script>
	
  </body>
</html>
