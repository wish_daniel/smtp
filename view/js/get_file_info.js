$(document).ready(function () {
	
	$.ajax({
        url:'../../controller/get_file_list.php',
        type:'POST',
        dataType:'jsonp',                        
        success:function(data){
			console.log(data);
			
			$('#output_filename_target_1').html(data['target_1']);
			$('#output_filename_target_2').html(data['target_2']);
			$('#output_filename_target_3').html(data['target_3']);
			$('#output_target_cnt_1').html(data['target_cnt_1']);
			$('#output_target_cnt_2').html(data['target_cnt_2']);
			$('#output_target_cnt_3').html(data['target_cnt_3']);
			$('#output_mail_title_1').html(data['title_1']);
			$('#output_mail_title_2').html(data['title_2']);
			$('#output_mail_title_3').html(data['title_3']);
			// $('#filename_weekly').html(data['weekly']);
			// $('#filename_monthly').html(data['monthly']);
        }
    })
	
});

$("#send_title_1").click(function(){

		var title = $('#input_mail_title_1').val();
		var type = 1;

		$.ajax({
		    url:'../../controller/set_data.php',
		    type:'POST',
		    dataType:'jsonp',                        
		    data:{
		    	'type':type,
		        'title':title,
		        },
		    success:function(data){
		    	console.log(data);
				$('#output_mail_title_1').html(data);
		    }
		})
});

$("#send_title_2").click(function(){

		var title = $('#input_mail_title_2').val();
		var type = 2;

		$.ajax({
		    url:'../../controller/set_data.php',
		    type:'POST',
		    dataType:'jsonp',                        
		    data:{
		    	'type':type,
		        'title':title,
		        },
		    success:function(data){
		    	console.log(data);
				$('#output_mail_title_2').html(data);
		    }
		})
});

$("#send_title_3").click(function(){

		var title = $('#input_mail_title_3').val();
		var type = 3;

		$.ajax({
		    url:'../../controller/set_data.php',
		    type:'POST',
		    dataType:'jsonp',                        
		    data:{
		    	'type':type,
		        'title':title,
		        },
		    success:function(data){
		    	console.log(data);
				$('#output_mail_title_3').html(data);
		    }
		})
});