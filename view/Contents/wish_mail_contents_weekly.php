<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko"  xml:lang="ko">
<meta http-equiv="Content-Type" Content="text/html; charset=utf-8" />
	<head>
		<title>Weekly Contents</title>
	</head>
	<body>
	<header>
		<a href="http://58.150.60.107/SMTP/view/Contents/wish_mail_contents_weekly.php"><button class="btn btn-lg btn-info" style="margin-left: 150px;">Weekly Mail</button></a>
		<a href="http://58.150.60.107/SMTP/view/Contents/wish_mail_contents_monthly.php"><button class="btn btn-lg btn-info">Monthly Mail</button></a>
	</header>	
		<div id="main_container">
			<div class="container">
			    <h1 class="well" style="text-align: center;">Weekly Mail Contents</h1>
			    <h6 class="well" style="text-align: center;">사용방법
			    										<br>각 칸에 상품번호를 입력합니다.
			    										<br>확인 버튼을 누릅니다.
			    										<br>해당 위치에 맞는 상품이 입력됩니다.
			    										</h1>
				<div class="col-lg-12 well">
				<div class="row">
					<div>
						<div class="col-sm-12">
							<div class="row">
								<div class="col-sm-4 form-group">
									<label>1_1</label>
									<!-- <input type="text" placeholder="Product Number" class="form-control" name="pid[]"> -->
									<input type="text" placeholder="Product Number" class="form-control" id="1_1ProductNumber">
									<button class="btn btn-lg btn-info" id="btn1_1">btn 1-1</button>
								</div>
								<div class="form-group col-sm-4">
									<label>1_2</label>
									<!-- <input type="text" placeholder="Product Number" class="form-control" name="pid[]"> -->
									<input type="text" placeholder="Product Number" class="form-control" id="1_2ProductNumber">
									<button class="btn btn-lg btn-info" id="btn1_2">btn 1-2</button>
								</div>
								<div class="form-group col-sm-4">
									<label>1_3</label>
									<!-- <input type="text" placeholder="Product Number" class="form-control" name="pid[]"> -->
									<input type="text" placeholder="Product Number" class="form-control" id="1_3ProductNumber">
									<button class="btn btn-lg btn-info" id="btn1_3">btn 1-3</button>
								</div>							
							</div>					
							<div class="row">
								<div class="col-sm-4 form-group">
									<label>3_1</label>
									<!-- <input type="text" placeholder="Product Number" class="form-control" name="pid[]"> -->
									<input type="text" placeholder="Product Number" class="form-control" id="3_1ProductNumber">
									<button class="btn btn-lg btn-info" id="btn3_1">btn 3_1</button>
								</div>
								<div class="form-group col-sm-4">
									<label>3_2</label>
									<!-- <input type="text" placeholder="Product Number" class="form-control" name="pid[]"> -->
									<input type="text" placeholder="Product Number" class="form-control" id="3_2ProductNumber">
									<button class="btn btn-lg btn-info" id="btn3_2">btn 3_2</button>
								</div>
								<div class="form-group col-sm-4">
									<label>3_3</label>
									<!-- <input type="text" placeholder="Product Number" class="form-control" name="pid[]"> -->
									<input type="text" placeholder="Product Number" class="form-control" id="3_3ProductNumber">
									<button class="btn btn-lg btn-info" id="btn3_3">btn 3_3</button>
								</div>		
							</div>
							<!-- <input class="btn btn-lg btn-info" type="submit" value="Submit" id="btn1_1"> -->
							<button class="btn btn-lg btn-info" id="html_down">download</button>					
						</div>
					</div>
					</div>
				</div>
			</div>
			
			<!-- Create Html Code Section -->
			<div id="html_main">
			    <table width="100%" border="0" cellpadding="0" cellspacing="0" style="font-family: Arial;">
                    <!--Header-->
                    <tr>
                        <td colspan="3" align="center">
                            <img src="http://www.wishtrend.com/img/cms/Mail/new/wishtrend_mail_header.jpg" usemap="#menu_map" border="0">
                            <map name="menu_map" id="menu_map">
                              <area shape="rect" coords="237,25,457,86" href="http://www.wishtrend.com/" target="_blanck"/>
                              <area shape="rect" coords="22,103,122,124" href="http://www.wishtrend.com/307-free-shipping-zone" target="_blanck"/>
                              <area shape="rect" coords="133,102,211,127" href="http://www.wishtrend.com/module/customer_extra_information/SteadySeller" target="_blanck"/>
                              <area shape="rect" coords="224,103,293,126" href="http://www.wishtrend.com/209-skin-care" target="_blanck"/>
                              <area shape="rect" coords="302,102,370,128" href="http://www.wishtrend.com/163-make-up" target="_blanck"/>
                              <area shape="rect" coords="373,103,422,126" href="http://www.wishtrend.com/218-hair" target="_blanck"/>
                              <area shape="rect" coords="428,103,472,126" href="http://www.wishtrend.com/178-body-care" target="_blanck"/>
                              <area shape="rect" coords="483,102,555,127" href="http://www.wishtrend.com/196-tool-accessories" target="_blanck"/>
                              <area shape="rect" coords="560,103,609,127" href="http://www.wishtrend.com/182-men" target="_blanck"/>
                              <area shape="rect" coords="614,102,675,128" href="http://www.wishtrend.com/260-special" target="_blanck"/>
                            </map>
                        </td>
                    <tr>
                    <!--Promotion banner-->
                    <tr>
                        <td colspan="3" align="center">
                            <a href="" target="_blank">
                                <img src="http://www.wishtrend.com/upload/stowlcarousel/e4117bdde3d22833ff33d79124ea8438.jpg" width="700"/>
                            </a>
                        </td>
                    </tr>
                    <!--Txt-->
                    <tr>
                        <td colspan="3" align="center">
                            <p style="padding:35px 0;">
                                <strong>Hello Beauty explorers !</strong>
                                <br />
                                We introduce to you our best & new items on this week
                                <br />
                                and big benefits that you may wait for it.
                                <br /><br />
                                <a href="http://www.wishtrend.com/" target="_blank">
                                        <img src="http://www.wishtrend.com/img/cms/Mail/new/btn_shoponwishtrend.jpg" />
                                </a>
                            </p>
                        </td>
                    </tr>
                    <!--Best 6-->
                    <tr>
                        <td colspan="3" align="center">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <!--Best 6 title-->
                            <tr>
                                <td width="30px" >&nbsp;</td>
                                <td width="640px" align="center" style="padding-top:45px;padding-bottom:35px;border-top:3px solid #eeeeee;">
                                    <p><img src="http://www.wishtrend.com/img/cms/Mail/new/best_title.jpg" /></p>
                                </td>
                                <td width="30px" >&nbsp;</td>
                            </tr>
                            <!--Best item line 1-->
                            <tr>
                                <td width="30px" >&nbsp;</td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <!--Item 1-->
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img id="img1_1" src="" alt="thumbnail" width="200px"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:16px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img src="http://www.wishtrend.com/img/cms/Mail/new/btn_shopnow.jpg" alt="shopnow"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:12px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td id="brand1_1">
                                                            <a href="" target="_blank">
                                                            brand
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="title1_1" style="width:200px;height:40px;">
                                                            <a href="" target="_blank">
                                                            product title
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:10px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span id="price1_1" style="display:inline;color:#909090;">original value</span>
                                                            &nbsp;
                                                            <span id="reduction1_1" style="display:inline;">price</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:6px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
															<div style="    margin-left: 0px;
															    margin-top: 0;
															    margin-bottom: 1px;
															    width: 100px;
															    height: 16px;
															    background: url('http://www.wishdevelop.com/themes/panda/css/modules/blockreviews/css/../images/rating_star.png') 0 -16px no-repeat;
															    text-indent: -999px;
															    overflow: hidden;">
																<span id="point1_1" style="display: block;"></span>
															</div>
														</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td rowspan="8" style="width:20px;">&nbsp;</td>
                                            <!--Item 2-->
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img id="img1_2" src="" alt="thumbnail" width="200px"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:16px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img src="http://www.wishtrend.com/img/cms/Mail/new/btn_shopnow.jpg" alt="shopnow"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:12px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td id="brand1_2">
                                                            <a href="" target="_blank">
                                                            brand
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="title1_2" style="width:200px;height:40px;">
                                                            <a href="" target="_blank">
                                                            product title
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:10px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span id="price1_2" style="display:inline;color:#909090;">original value</span>
                                                            &nbsp;
                                                            <span id="reduction1_2" style="display:inline;">price</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:6px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
															<div style="    margin-left: 0px;
															    margin-top: 0;
															    margin-bottom: 1px;
															    width: 100px;
															    height: 16px;
															    background: url('http://www.wishdevelop.com/themes/panda/css/modules/blockreviews/css/../images/rating_star.png') 0 -16px no-repeat;
															    text-indent: -999px;
															    overflow: hidden;">
																<span id="point1_2" style="width:100%;display: block;"></span>
															</div>
														</td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td rowspan="8" style="width:20px;">&nbsp;</td>
                                            <!--Item 3-->
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img id="img1_3" src="" alt="thumbnail" width="200px"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:16px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img src="http://www.wishtrend.com/img/cms/Mail/new/btn_shopnow.jpg" alt="shopnow"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:12px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td id="brand1_3">
                                                            <a href="" target="_blank">
                                                            brand
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="title1_3" style="width:200px;height:40px;">
                                                            <a href="" target="_blank">
                                                            product title
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:10px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span id="price1_3" style="display:inline;color:#909090;">original value</span>
                                                            &nbsp;
                                                            <span id="reduction1_3" style="display:inline;">price</span>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:6px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
															<div style="    margin-left: 0px;
															    margin-top: 0;
															    margin-bottom: 1px;
															    width: 100px;
															    height: 16px;
															    background: url('http://www.wishdevelop.com/themes/panda/css/modules/blockreviews/css/../images/rating_star.png') 0 -16px no-repeat;
															    text-indent: -999px;
															    overflow: hidden;">
																<span id="point1_3" style="width:100%;display: block;"></span>
																
															</div>
														</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="30px" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="30px" >&nbsp;</td>
                                <td height="50px">&nbsp;</td>
                                <td width="30px" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="30px" >&nbsp;</td>
                                <td height="70px" style="border-bottom:3px solid #eeeeee;">&nbsp;</td>
                                <td width="30px" >&nbsp;</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <!--new 3-->
                    <tr>
                        <td colspan="3" align="center">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <!--New 3 title-->
                            <tr>
                                <td width="30px" >&nbsp;</td>
                                <td width="640px" align="center" style="padding-top:60px;padding-bottom:35px;border-top:3px solid #eeeeee;">
                                    <p><img src="http://www.wishtrend.com/img/cms/Mail/new/new_title.jpg" /></p>
                                </td>
                                <td width="30px" >&nbsp;</td>
                            </tr>
                            <!--New item line 1-->
                            <tr>
                                <td width="30px" >&nbsp;</td>
                                <td>
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <!--Item 1-->
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img id="img3_1" src="" alt="thumbnail" width="200px"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:16px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img src="http://www.wishtrend.com/img/cms/Mail/new/btn_shopnow.jpg" alt="shopnow"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:12px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td id="brand3_1">
                                                            <a href="" target="_blank">
                                                            brand
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="title3_1" style="width:200px;height:40px;">
                                                            <a href="" target="_blank">
                                                            product title
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:10px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span id="price3_1" style="display:inline;color:#909090;">original value</span>
                                                            &nbsp;
                                                            <span id="reduction3_1" style="display:inline;">price</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td rowspan="8" style="width:20px;">&nbsp;</td>
                                            <!--Item 2-->
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img id="img3_2" src="" alt="thumbnail" width="200px"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:16px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img src="http://www.wishtrend.com/img/cms/Mail/new/btn_shopnow.jpg" alt="shopnow"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:12px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td id="brand3_2">
                                                            <a href="" target="_blank">
                                                            brand
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="title3_2" style="width:200px;height:40px;">
                                                            <a href="" target="_blank">
                                                            product title
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:10px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span id="price3_2" style="display:inline;color:#909090;">original value</span>
                                                            &nbsp;
                                                            <span id="reduction3_2" style="display:inline;">price</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td rowspan="8" style="width:20px;">&nbsp;</td>
                                            <!--Item 3-->
                                            <td>
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img id="img3_3" src="" alt="thumbnail" width="200px"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:16px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img src="http://www.wishtrend.com/img/cms/Mail/new/btn_shopnow.jpg" alt="shopnow"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:12px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td id="brand3_3">
                                                            <a href="" target="_blank">
                                                            brand
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td id="title3_3" style="width:200px;height:40px;">
                                                            <a href="" target="_blank">
                                                            product title
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:10px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <span id="price3_3" style="display:inline;color:#909090;">original value</span>
                                                            &nbsp;
                                                            <span id="reduction3_3" style="display:inline;">price</span>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="30px" >&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="30px" >&nbsp;</td>
                                <td height="70px">&nbsp;</td>
                                <td width="30px" >&nbsp;</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                    <!--Contents-->
                    <tr>
                        <td colspan="3" align="center">
                        <table border="0" cellpadding="0" cellspacing="0">
                            <!--Contents title-->
                            <tr>
                                <td width="30px" bgcolor="#eeeeee">&nbsp;</td>
                                <td width="640px" align="center" style="padding-top:60px;padding-bottom:35px;background-color:#eeeeee;">
                                    <p><img src="http://www.wishtrend.com/img/cms/Mail/new/contents_title.png" /></p>
                                </td>
                                <td width="30px" bgcolor="#eeeeee">&nbsp;</td>
                            </tr>
                            <!--New item line 1-->
                            <tr>
                                <td width="30px" bgcolor="#eeeeee">&nbsp;</td>
                                <td style="background-color:#eeeeee;">
                                    <table border="0" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <!--Item 1-->
                                            <td width="300px;">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <a href="http://www.wishtrend.com/glam/category/article/trend-and-news/" target="_blank">
                                                            <img src="http://www.wishtrend.com/glam/wp-content/uploads/2016/03/COVER7-680x350.jpg" alt="thumbnail" width="300px" height="168px"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:16px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-bottom:1px solid #000000;font-size:14px;">
                                                            TREND & NEWS
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:12px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight:700;">
                                                            <a style="text-decoration:none;color:black;font-size:14px;" href="http://www.wishtrend.com/glam/gentle-acne-treatment-with-blue-cream/" target="_blank">
                                                            WIGS FOR WOMEN IS 2016 BIGGEST HAIR TREND
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size:14px;">
                                                            Hello Friends, Today we discuss a really interesting topic which draws our attention to one of 2016 ...
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:10px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="http://www.wishtrend.com/glam/category/article/trend-and-news/" target="_blank">
                                                            <img src="http://www.wishtrend.com/img/cms/Mail/new/btn_readmore.jpg" alt="read more" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td rowspan="8" style="width:40px;">&nbsp;</td>
                                            <!--Item 2-->
                                            <td width="300px;">
                                                <table border="0" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>
                                                            <a href="" target="_blank">
                                                            <img src="http://www.wishtrend.com/glam/wp-content/uploads/2016/03/COVER2.jpg" alt="thumbnail" width="300px" height="168px"/>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:16px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-bottom:1px solid #000000;font-size:14px;">
                                                            TIPS & TUTORIALS
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:12px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-weight:700;">
                                                            <a style="text-decoration:none;color:black;font-size:14px;" href="http://www.wishtrend.com/glam/gentle-acne-treatment-with-blue-cream/" target="_blank">
                                                            GENTLE ACNE TREATMENT WITH BLUE CREAM
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size:14px;">
                                                            Hello Lovely People,

How are you? It&apos;s been a long weekend and I have some very exiting information ...
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="height:10px;">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <a href="http://www.wishtrend.com/glam/category/article/trend-and-news/" target="_blank">
                                                            <img src="http://www.wishtrend.com/img/cms/Mail/new/btn_readmore.jpg" alt="read more" />
                                                            </a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="30px" bgcolor="#eeeeee">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="30px" bgcolor="#eeeeee">&nbsp;</td>
                                <td height="60px" style="background-color:#eeeeee;">&nbsp;</td>
                                <td width="30px" bgcolor="#eeeeee">&nbsp;</td>
                            </tr>
                        </table>
                        </td>
                    </tr>
                
                    <!--Footer-->
                    <tr>
                      <td colspan="3" align="center">
                        <img src="http://www.wishtrend.com/img/cms/Mail/new/wishtrend_mail_footer.jpg" width="700" usemap="#footer_map" border="0" />
                        <map name="footer_map" id="footer_map">
                          <area shape="circle" coords="335,35,21" href="https://www.youtube.com/user/WISHTrendTV" target="_blank"/>
                          <area shape="circle" coords="398,35,19" href="https://twitter.com/wishtrend" target="_blank"/>
                          <area shape="circle" coords="460,36,19" href="https://kr.pinterest.com/wishtrend/" target="_blank"/>
                          <area shape="circle" coords="523,35,19" href="https://www.facebook.com/WISHglobal/" target="_blank"/>
                          <area shape="circle" coords="586,35,19" href="https://www.instagram.com/wishtrend/" target="_blank"/>
                        </map>
                      </td>
                    </tr>
                </table>
			</div>
		</div>

	<!-- jquery CDN 적용 -->
	<script src="https://code.jquery.com/jquery-2.2.0.js"></script>
	<!-- 부트스트랩 사용하기위한 CDN -->
	<!-- 합쳐지고 최소화된 최신 CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<!-- 부가적인 테마 -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">    
	<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
	<!-- daniel's css -->
	<link rel="stylesheet" href="../css/MailingBO.css">
	<!-- daniel's script code -->
	<script type="text/javascript" src="../../js/make_mailing.js"></script>

	</body>
</html>