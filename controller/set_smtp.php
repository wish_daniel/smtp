<?php

//데이터베이스 연결 및 데이터베이스 선택
$connect = mysqli_connect("localhost","root","3412Creative^^","user_list");

//데이터베이스 연결 예외처리
if (mysqli_connect_errno($connect)) {
	echo "데이터베이스 연결 오류:".mysqli_connect_error();
}

class SMTP {

	/***********************************************
	 * 
	 * count value
	 * Add/Reset/Branch
	 * 
	 * *********************************************/

	function set_count($connect){
		//set count value
		$set_count_query = "UPDATE `count_table` SET `chk_cnt_1`=0, `chk_cnt_2`=0, `chk_cnt_3`=0 WHERE `index`=1";
		mysqli_query($connect,$set_count_query) or die("update error");	
	}
	
	
	function get_count($connect,$target_num){
		//get count value
		$get_count_query = 'SELECT chk_cnt_'.$target_num.' FROM count_table WHERE 1';
		$get_count = mysqli_query($connect,$get_count_query) or die("count ".$target_num." table get error");
		$count_chk = mysqli_fetch_row($get_count);
		
		return $count_chk;	
	}
	
	
	function add_count($connect,$target_num){
		//add count query
		$add_count_querry = "UPDATE `count_table` SET `chk_cnt_".$target_num."`= `chk_cnt_".$target_num."`+1 WHERE `index`=1";
		mysqli_query($connect,$add_count_querry) or die("chk_cnt add error");	
	}	
	
	
	function get_cri($connect){
		//get cri query
		$get_cri_query = 'SELECT cri_cnt FROM count_table WHERE 1';
		$get_cri = mysqli_query($connect,$get_cri_query) or die("cri get error");
		$cri_cnt = mysqli_fetch_row($get_cri);
		
		return $cri_cnt;	
	}


	/***********************************************
	 * 
	 * cron value.
	 * Get/Set/Stop/Add/Branch/Compare
	 * 
	 * *********************************************/
	
	function stop_cron($connect){
		//set cron value
		$stop_cron_query = 'UPDATE `shell_checker` SET `cron_chk`=12 WHERE `sh_chk_index`=1';
		mysqli_query($connect,$stop_cron_query) or die("1");	
	}
	
	function set_cron($connect){
		//set cron value
		$set_cron_query = 'UPDATE `shell_checker` SET `cron_chk`=0 WHERE `sh_chk_index`=1';
		mysqli_query($connect,$set_cron_query) or die("2");	
	}
	
	function get_cron($connect){
		//get cron value
		$get_cron_query = 'SELECT cron_chk FROM shell_checker WHERE 1';
		$get_cron = mysqli_query($connect,$get_cron_query) or die("3");
		$cron_chk = mysqli_fetch_row($get_cron);
		
		return $cron_chk; 	
	}
	
	function add_cron($connect){
		//cron add query
		$add_cron_querry = "UPDATE `shell_checker` SET `cron_chk`=cron_chk+1 WHERE `sh_chk_index`=1";
		mysqli_query($connect,$add_cron_querry) or die("4");	
	}
	
	//해당 함수 사용 안할수도있다.	
	function compare_cron($connect,$cron_chk){
		if($cron_chk >= 12){
			// $mv = 'mv /var/www/html/SMTP/uploads/html_contents_1/'.$target_1.' /var/www/html/SMTP/uploads/send_contents';
			// shell_exec($mv);
			$return_val = true;
		}else{
			$return_val = false;			
		}
		
		return $return_val;
	}
	
	
	/***********************************************
	 * 
	 * shell value.
	 * Get/Reset/Branch
	 * 
	 * *********************************************/
	
	function set_shell($connect){
		//set shell checker value
		$set_cron_query = 'UPDATE `shell_checker` SET `target_shell_chk_1`=0, `target_shell_chk_2`=0, `target_shell_chk_3`=0 WHERE `sh_chk_index`=1';
		mysqli_query($connect,$set_cron_query) or die("5");	
	}
	
	
	function get_shell($connect,$target_num){
		//get shell checker value
		$get_shell_query = 'SELECT target_shell_chk_'.$target_num.' FROM shell_checker WHERE 1';
		$get_shell = mysqli_query($connect,$get_shell_query) or die("6");
		$shell_chk = mysqli_fetch_row($get_shell);
		
		return $shell_chk; 
	}
	
	
	function add_shell($connect,$target_num){
		//shell checker add query
		$add_cron_querry = "UPDATE `shell_checker` SET `target_shell_chk_".$target_num."`= `target_shell_chk_".$target_num."`+1 WHERE `sh_chk_index`=1";
		mysqli_query($connect,$add_cron_querry) or die("7");	
	}
	
	
	function compare_shell($connect,$target_num){
		
		$shell_chk = SMTP::get_shell($connect,$target_num)[0];
		
		if($shell_chk != 1){
			//to do
		} else if($shell_chk == 1) {
			//to do
		}
	}
	
	/***********************************************
	 * 
	 * send_table
	 * Get/Set/Branch
	 * 
	 * *********************************************/
	
	function all_set_sendtable($connect){
		//set all sendtable checker value
		$set_sendtable_query = 'truncate send_table_1';
		mysqli_query($connect,$set_sendtable_query) or die("8");
		
		$set_sendtable_query = 'truncate send_table_2';
		mysqli_query($connect,$set_sendtable_query) or die("9");
		
		$set_sendtable_query = 'truncate send_table_3';
		mysqli_query($connect,$set_sendtable_query) or die("10");	
	}
	
	
	function set_sendtable($connect,$target_num){
		//set sendtable checker value
		$set_sendtable_query = 'truncate send_table_'.$target_num;
		mysqli_query($connect,$set_sendtable_query) or die("11");	
	}
	
	
	function get_sendtable($connect,$target_num){
		//get sendtable checker value
		$get_sendtable_query = 'SELECT count(*) FROM send_table_'.$target_num.' WHERE 1';
		$get_sendtable = mysqli_query($connect,$get_sendtable_query) or die("12");
		$sendtable_cnt = mysqli_fetch_row($get_sendtable);
		
		return $sendtable_cnt; 
	}
	
	/***********************************************
	 * 
	 * Password.
	 * Get/Reset/Branch
	 * 
	 * *********************************************/
	
	function set_pass($connect,$password){
		//set password
		$set_cron_query = 'UPDATE `password` SET `password`='.$password.' WHERE `pass_index`=1';
		echo $set_cron_query;
		mysqli_query($connect,$set_cron_query) or die("13");	
	}
	
	
	function get_pass($connect){
		//get password
		$get_pass_query = 'SELECT password FROM password WHERE pass_index=1';
		$get_pass = mysqli_query($connect,$get_pass_query) or die("14");
		$password = mysqli_fetch_row($get_pass);
		return $password; 
	}	
	
	/***********************************************
	 * 
	 * Mail Analysis Send and truncate log.
	 * 
	 * *********************************************/
	
	function analysis_1(){
		shell_exec('cat /var/log/mail.log | /usr/sbin/pflogsumm | mail -s "Mail Statistics_target_1" victor@wishcompany.net');
		shell_exec('cat /var/log/mail.log | /usr/sbin/pflogsumm | mail -s "Mail Statistics_target_1" growingpain89@gmail.com');
		shell_exec('cat /dev/null > /var/log/mail.log');
	}
	
	function analysis_2(){
		shell_exec('cat /var/log/mail.log | /usr/sbin/pflogsumm | mail -s "Mail Statistics_target_2" victor@wishcompany.net');
		shell_exec('cat /var/log/mail.log | /usr/sbin/pflogsumm | mail -s "Mail Statistics_target_2" growingpain89@gmail.com');
		shell_exec('cat /dev/null > /var/log/mail.log');
	}
	
	function analysis_3(){
		shell_exec('cat /var/log/mail.log | /usr/sbin/pflogsumm | mail -s "Mail Statistics_target_3" victor@wishcompany.net');
		shell_exec('cat /var/log/mail.log | /usr/sbin/pflogsumm | mail -s "Mail Statistics_target_3" growingpain89@gmail.com');
		shell_exec('cat /dev/null > /var/log/mail.log');
	}
	
	function delete_log(){
		shell_exec('cat /dev/null > /var/log/mail.log');
	}
	
}

	// $pass='0000';
	// SMTP::set_count($connect);
	// SMTP::stop_cron($connect);
	// SMTP::set_shell($connect);
	// SMTP::set_pass($connect,$pass);

?>